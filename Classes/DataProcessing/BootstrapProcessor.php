<?php
namespace T3SBS\T3sbootstrap\DataProcessing;

/*
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */

use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Service\FlexFormService;
use TYPO3\CMS\Core\Page\PageRenderer;
use TYPO3\CMS\Core\Resource\FileRepository;
use TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer;
use TYPO3\CMS\Frontend\ContentObject\DataProcessorInterface;
use TYPO3\CMS\Frontend\Page\PageRepository;
use TYPO3\CMS\Frontend\Resource\FilePathSanitizer;
use T3SBS\T3sbootstrap\Utility\YouTubeRenderer;
use T3SBS\T3sbootstrap\Utility\BackgroundImageUtility;



class BootstrapProcessor implements DataProcessorInterface
{

	/**
	 * The content object renderer
	 *
	 * @var \TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer
	 */
	protected $contentObjectRenderer;

	/**
	 * The contentObject Configuration
	 *
	 * @var array
	 */
	protected $contentObjectConfiguration;

	/**
	 * The processor configuration
	 *
	 * @var array
	 */
	protected $processorConfiguration;

	/**
	 * The processedData
	 *
	 * @var array
	 */
	protected $processedData;

	/**
	 * The flexconf
	 *
	 * @var array
	 */
	protected $flexconf;

	/**
	 * The parentflexconf
	 *
	 * @var array
	 */
	protected $parentflexconf;

	/**
	 * Video in background wrapper
	 *
	 * @var bool
	 */
	protected $isVideo = FALSE;


	/**
	 * Process data
	 *
	 * @param ContentObjectRenderer $cObj The data of the content element or page
	 * @param array $contentObjectConfiguration The configuration of Content Object
	 * @param array $processorConfiguration The configuration of this processor
	 * @param array $processedData Key/value store of processed data (e.g. to be passed to a Fluid View)
	 * @return array the processed data as key/value store
	 */
	public function process(ContentObjectRenderer $cObj, array $contentObjectConfiguration, array $processorConfiguration,	 array $processedData)
	{
		$flexFormService = GeneralUtility::makeInstance(FlexFormService::class);
		$this->flexconf = $flexFormService->convertFlexFormContentToArray($processedData['data']['tx_t3sbootstrap_flexform']);
		$this->parentflexconf = $flexFormService->convertFlexFormContentToArray($processedData['data']['parentgrid_tx_t3sbootstrap_flexform']);
		$this->contentObjectRenderer = $cObj;
		$this->contentObjectConfiguration = $contentObjectConfiguration;
		$this->processorConfiguration = $processorConfiguration;
		$this->processedData = $processedData;

		##############################################################################################################################################
		/**
		 * CType: Gridelements
		 */
		##############################################################################################################################################
		if ($this->processedData['data']['CType'] == 'gridelements_pi1') {

			if ( is_array($this->processedData['data']['tx_gridelements_view_children']) ) {
				$this->processedData['divWrap'] = FALSE;
			} else {
				$this->processedData['divWrap'] = TRUE;
			}

			/**
			 * Tabs / Pills
			 */
			if ( $this->processedData['data']['tx_gridelements_backend_layout'] == 'tabs_container'
			 || $this->processedData['data']['tx_gridelements_backend_layout'] == 'tabs_tab' ) {

				if ( $this->flexconf['display_type'] == 'verticalpills') {
					$this->processedData['pill']['asideWidth'] = (int)$this->flexconf['aside_width'];

					switch ( $this->flexconf['aside_width'] ) {
						 case 1:
							$this->processedData['pill']['mainWidth'] = 11;
						break;
						 case 2:
							$this->processedData['pill']['mainWidth'] = 10;
						break;
						 case 3:
							$this->processedData['pill']['mainWidth'] = 9;
						break;
						 case 4:
							$this->processedData['pill']['mainWidth'] = 8;
						break;
						 case 6:
							$this->processedData['pill']['mainWidth'] = 6;
						break;
								 default:
							$this->processedData['pill']['mainWidth'] = 9;
					}
				}
				$this->processedData['tab']['displayType'] = $this->flexconf['display_type'];
				$this->processedData['tab']['switchEffect'] =  $this->parentflexconf['switch_effect'];
				$this->processedData['tab']['contentByPid'] =  $this->flexconf['contentByPid'];
				$this->processedData['tab']['fill'] =  $this->flexconf['fill'] ? ' '.$this->flexconf['fill']: '';
			}

			/**
			 * Card Wrapper
			 */
			if ( $this->processedData['data']['tx_gridelements_backend_layout'] == 'card_wrapper' ) {
				if (is_array($this->processedData['data']['tx_gridelements_view_children'])) {
					$fileRepository = GeneralUtility::makeInstance(FileRepository::class);
					foreach ( $this->processedData['data']['tx_gridelements_view_children'] as $key=>$child ) {
						$fileObjects = $fileRepository->findByRelation('tt_content', 'assets', $child['uid']);
						$children[$key] = $flexFormService->convertFlexFormContentToArray($child['pi_flexform']);
						$children[$key]['uid'] = $child['uid'];
						$children[$key]['card-header'] = $child['header'];
						$children[$key]['card-subheader'] = $child['subheader'];
						$children[$key]['header_link'] = $child['header_link'];
						$children[$key]['tx_t3sbootstrap_header_display'] = $child['tx_t3sbootstrap_header_display'];
						$children[$key]['header_position'] = $child['header_position'];
						$children[$key]['tx_t3sbootstrap_header_class'] = $child['tx_t3sbootstrap_header_class'];
						$children[$key]['tx_t3sbootstrap_header_fontawesome'] = $child['tx_t3sbootstrap_header_fontawesome'];
						$children[$key]['settings'] = $flexFormService->convertFlexFormContentToArray($child['tx_t3sbootstrap_flexform']);
						$children[$key]['file'] = $fileObjects[0];
					}
					$this->processedData['cards'] = $children;

					if ($this->flexconf['card_wrapper'] == 'flipper'){
						switch ( count($this->processedData['data']['tx_gridelements_view_children']) ) {
							 case 1:
								$this->processedData['flipper']['class'] = 'col-xs-12 col-sm-12 col-md-12';
							break;
							 case 2:
								$this->processedData['flipper']['class'] = 'col-xs-12 col-sm-6 col-md-6';
							break;
							 case 3:
								$this->processedData['flipper']['class'] = 'col-xs-12 col-sm-6 col-md-4';
							break;
							 case 4:
								$this->processedData['flipper']['class'] = 'col-xs-12 col-sm-6 col-md-3';
							break;
							 case 6:
								$this->processedData['flipper']['class'] = 'col-xs-12 col-sm-6 col-md-2';
							break;
									 default:
								$this->processedData['flipper']['class'] = 'col-xs-12 col-sm-6 col-md-4';
						}
					}
				}
				$this->processedData['card_wrapper_layout'] = $this->flexconf['card_wrapper'] ?: '';
			}

			/**
			 * Background Wrapper
			 */
			if ( $this->processedData['data']['tx_gridelements_backend_layout'] == 'background_wrapper' && $this->processedData['data']['assets'] ) {

				$fileRepository = GeneralUtility::makeInstance(FileRepository::class);
				$fileObjects = $fileRepository->findByRelation('tt_content', 'assets', $this->processedData['data']['uid']);
				$file = $fileObjects[0];

				if ($this->flexconf['origImage']) {
					$this->processedData['file'] = $file;
				}

				if ( $file->getType() === 4 && ($file->getMimeType() === 'video/youtube' || $file->getExtension() === 'youtube') ) {

					$this->isVideo = TRUE;
					$this->processedData['contentCenter'] = $this->flexconf['contentCenter'] ? TRUE : FALSE;

						$filter = $this->flexconf['grayscale'] ? 'grayscale: '.(int)$this->flexconf['grayscale'].', ' : '';
						$filter .= $this->flexconf['huerotate'] ? 'hue_rotate: '.(int)$this->flexconf['huerotate'].', ' : '';
						$filter .= $this->flexconf['invert'] ? 'invert: '.(int)$this->flexconf['invert'].', ' : '';
						$filter .= $this->flexconf['opacity'] ? 'opacity: '.(int)$this->flexconf['opacity'].', ' : '';
						$filter .= $this->flexconf['saturate'] ? 'saturate: '.(int)$this->flexconf['saturate'].', ' : '';
						$filter .= $this->flexconf['sepia'] ? 'sepia: '.(int)$this->flexconf['sepia'].', ' : '';
						$filter .= $this->flexconf['brightness'] ? 'brightness: '.(int)$this->flexconf['brightness'].', ' : '';
						$filter .= $this->flexconf['contrast'] ? 'contrast: '.(int)$this->flexconf['contrast'].', ' : '';
						$filter .= $this->flexconf['blur'] ? 'blur: '.(int)$this->flexconf['blur'].', ' : '';

					$filter = substr(trim($filter), 0, -1);
					if ( $filter ) {
						$addFilters = ' var filters = {'.$filter.'}; jQuery(\'.player'.$this->processedData['data']['uid'].'\').YTPApplyFilters(filters);';
					} else {
						$addFilters = '';
					}

					$cssFile = 'EXT:t3sbootstrap/Resources/Public/Styles/jquery.mb.YTPlayer.min.css';
					$cssFile = GeneralUtility::makeInstance(FilePathSanitizer::class)->sanitize($cssFile);

					if ($this->flexconf['videoRatio'] == '16/9' || $this->flexconf['videoRatio'] == 'auto') {
						$pH = ((int)$this->flexconf['bgHeight'] / 16) * 9;
					} else {
						$pH = ((int)$this->flexconf['bgHeight'] / 4) * 3;
					}

					$jsFooterFile = 'EXT:t3sbootstrap/Resources/Public/Contrib/jquery.mb.YTPlayer.min.js';
					$jsFooterFile = GeneralUtility::makeInstance(FilePathSanitizer::class)->sanitize($jsFooterFile);
					if ( $this->flexconf['bgHeight'] ) {
					$inlineJS = 'jQuery(function(){
						jQuery(\'.player'.$this->processedData['data']['uid'].'\').YTPlayer({ realfullscreen: true, onReady: function(event) { $(\'body\').addClass(\'video-loaded\');}});'.$addFilters.'
						jQuery(\'.player'.$this->processedData['data']['uid'].'\').css("padding-bottom", "'.$pH.'%")
					});';
					} else {
					$inlineJS = 'jQuery(function(){
						jQuery(\'.player'.$this->processedData['data']['uid'].'\').YTPlayer({ realfullscreen: true, onReady: function(event) { $(\'body\').addClass(\'video-loaded\');}});'.$addFilters.'
					});';
					}
					$pageRenderer = GeneralUtility::makeInstance(PageRenderer::class);
					$pageRenderer->addCssFile($cssFile);
					$pageRenderer->addJsFooterFile($jsFooterFile);
					$pageRenderer->addJsFooterInlineCode(' Background-video ',$inlineJS,'FALSE');

					$events = $this->flexconf;
					$events['videoAutoPlay'] = $file->getProperties()['autoplay'];
					$events['uid'] = $this->processedData['data']['uid'];

					$this->processedData['youtubeProperty'] = GeneralUtility::makeInstance(YouTubeRenderer::class)->render($file, $events);

				} else {
					if ( $file->getType() === 4 && ($file->getMimeType() === 'video/vimeo' || $file->getExtension() === 'vimeo') ) {
						$this->processedData['vimeo'] = TRUE;
					} else {
						$this->processedData['bgImage'] = GeneralUtility::makeInstance(BackgroundImageUtility::class)->getBgImage($this->processedData['data']['uid']);
						$this->processedData['imageRaster'] = $this->flexconf['imageRaster'] ? 'multiple-' : '';
					}
				}
			}

			/**
			 * Parallax Wrapper
			 */
			if ( $this->processedData['data']['tx_gridelements_backend_layout'] == 'parallax_wrapper' && $this->processedData['data']['assets'] ) {
				$fileRepository = GeneralUtility::makeInstance(FileRepository::class);
				$fileObjects = $fileRepository->findByRelation('tt_content', 'assets', $this->processedData['data']['uid']);
				$file = $fileObjects[0];
				if ( $file->getType() === 4 ) {
					$this->processedData['video'] = TRUE;
				} else {
					$this->processedData['parallaxImage'] = GeneralUtility::makeInstance(BackgroundImageUtility::class)->getBgImage($this->processedData['data']['uid']);
					$this->processedData['speedFactor'] = $this->flexconf['speedFactor'];
					$this->processedData['imageRaster'] = $this->flexconf['imageRaster'] ? 'multiple-' : '';
				}
			}

			/**
			 * Carousel container
			 */
			if ( $this->processedData['data']['tx_gridelements_backend_layout'] == 'carousel_container' ) {

				$this->processedData['interval'] = $this->flexconf['interval'];
				$this->processedData['carouselFade'] = $this->flexconf['carouselFade'] ? ' carousel-fade': '';
			}

			/**
			 * Button group
			 */
			if ( $this->processedData['data']['tx_gridelements_backend_layout'] == 'button_group' ) {
				$this->processedData['buttonGroupAlign'] = $this->flexconf['align'] ?: '';
			}

			/**
			 * Collapse Container
			 */
			if ( $this->processedData['data']['tx_gridelements_backend_layout'] == 'collapsible_container' ) {
				$this->processedData['appearance'] = $this->flexconf['appearance'];
			}

			/**
			 * Collapse
			 */
			if ( $this->processedData['data']['tx_gridelements_backend_layout'] == 'collapsible_accordion' ) {

				$fileRepository = GeneralUtility::makeInstance(FileRepository::class);
				$fileObjects = $fileRepository->findByRelation('tt_content', 'assets', $this->processedData['data']['uid']);
				$file = $fileObjects[0];

				$this->processedData['appearance'] = $this->parentflexconf['appearance'];
				$this->processedData['show'] = $this->flexconf['active'] ? ' show' : '';
				$this->processedData['expanded'] = $this->flexconf['active'] ? 'true' : 'false';
				$this->processedData['buttonstyle'] = $this->flexconf['style'] ? $this->flexconf['style'] : 'primary';
				$this->processedData['media'] = $file ? $file : '';
			}

			/**
			 * Grid
			 */
			if ( $this->processedData['data']['tx_gridelements_backend_layout'] == 'two_columns'
			 || $this->processedData['data']['tx_gridelements_backend_layout'] == 'three_columns'
			 || $this->processedData['data']['tx_gridelements_backend_layout'] == 'four_columns'
			 || $this->processedData['data']['tx_gridelements_backend_layout'] == 'six_columns' ) {

				foreach ($this->flexconf as $key=>$grid) {

					if ( $key == 'extraClass_one' || $key == 'extraClass_two' || $key == 'extraClass_three'
					 || $key == 'extraClass_four' || $key == 'extraClass_five' || $key == 'extraClass_six' ) {

						switch ( $key ) {
							 case 'extraClass_one':
							 	$columnOneExtraClass = ' '.$grid;
							break;
							 case 'extraClass_two':
							 	$columnTwoExtraClass = ' '.$grid;
							break;
							 case 'extraClass_three':
							 	$columnThreeExtraClass = ' '.$grid;
							break;
							 case 'extraClass_four':
							 	$columnFourExtraClass = ' '.$grid;
							break;
							 case 'extraClass_five':
							 	$columnFiveExtraClass = ' '.$grid;
							break;
							 case 'extraClass_six':
							 	$columnSixExtraClass = ' '.$grid;
							break;
						}
					}
				}

				if ( $this->flexconf['equalWidth'] ) {

					$colOne = 'col';
					$colTwo = 'col';
					$colThree = 'col';
					$colFour = 'col';
					$colFive = 'col';
					$colSix = 'col';

				} else {

					foreach ($this->flexconf as $key=>$grid) {

						if ( substr($key, 0, 2) != 'ex' ) {

							if ( $key != 'extraClass_one' || $key != 'extraClass_two' || $key != 'extraClass_three'
							 || $key != 'extraClass_four' || $key != 'extraClass_five' || $key != 'extraClass_six' ) {

								if ($grid != '0') {
									if ( substr($key, 0, 2) == 'xs' ) {
										if ( substr($key, -3) == 'one' ) {
											$columnOne .= ' col-'.$grid;
										}
										if ( substr($key, -3) == 'two' ) {
											$columnTwo .= ' col-'.$grid;
										}
										if ( substr($key, -5) == 'three' ) {
											$columnThree .= ' col-'.$grid;
										}
										if ( substr($key, -4) == 'four' ) {
											$columnFour .= ' col-'.$grid;
										}

										if ( substr($key, -4) == 'five' ) {
											$columnFive .= ' col-'.$grid;
										}
										if ( substr($key, -3) == 'six' ) {
											$columnSix .= ' col-'.$grid;
										}

									} else {
										if ( substr($key, -3) == 'one' ) {
											$columnOne .= ' col-'.substr($key, 0, -4).'-'.$grid;
										}
										if ( substr($key, -3) == 'two' ) {
											$columnTwo .= ' col-'.substr($key, 0, -4).'-'.$grid;
										}
										if ( substr($key, -5) == 'three' ) {
											$columnThree .= ' col-'.substr($key, 0, -6).'-'.$grid;
										}
										if ( substr($key, -4) == 'four' ) {
											$columnFour .= ' col-'.substr($key, 0, -5).'-'.$grid;
										}

										if ( substr($key, -4) == 'five' ) {
											$columnFive .= ' col-'.substr($key, 0, -5).'-'.$grid;
										}
										if ( substr($key, -3) == 'six' ) {
											$columnSix .= ' col-'.substr($key, 0, -4).'-'.$grid;
										}
									}
								}
							}
						}
					}

					$colOne = $columnOne;
					$colTwo = $columnTwo;
					$colThree = $columnThree;
					$colFour = $columnFour;
					$colFive = $columnFive;
					$colSix = $columnSix;
				}

				$this->processedData['columnOne'] = trim($colOne.$columnOneExtraClass);
				$this->processedData['columnTwo'] = trim($colTwo.$columnTwoExtraClass);
				$this->processedData['columnThree'] = trim($colThree.$columnThreeExtraClass);
				$this->processedData['columnFour'] = trim($colFour.$columnFourExtraClass);
				$this->processedData['columnFive'] = trim($colFive.$columnFiveExtraClass);
				$this->processedData['columnSix'] = trim($colSix.$columnSixExtraClass);

			}

			/**
			 * Modal
			 */
			if ( $this->processedData['data']['tx_gridelements_backend_layout'] == 'modal' ) {
					$this->processedData['modal']['animation'] = $this->flexconf['animation'];
					$this->processedData['modal']['size'] = $this->flexconf['size'];
					$this->processedData['modal']['button'] = $this->flexconf['button'];
					$this->processedData['modal']['style'] = $this->flexconf['style'];
					$this->processedData['modal']['ptag'] = $this->flexconf['ptag'];
			}

			$class = self::getGeClass();

		##############################################################################################################################################
		} else {
		##############################################################################################################################################

			$this->processedData['divWrap'] = $this->processedData['data']['CType'] == 'shortcut' ? FALSE : TRUE;

			/**
			 * Button
			 */
			if ( $this->processedData['data']['CType'] == 't3sbs_button' ) {
				$outline = $this->flexconf['outline'] ? 'outline-':'';
				$typolinkButtonClass = 'btn btn-'.$outline.$this->flexconf['style'];

				// if is child of gridelement (button_group)
				if ( $this->processedData['data']['parentgrid_tx_gridelements_backend_layout'] == 'button_group' ) {
					$this->processedData['divWrap'] = FALSE;
				} else {
					$typolinkButtonClass .= $this->flexconf['size'] ? ' '.$this->flexconf['size']:'';
					$typolinkButtonClass .= $this->flexconf['block'] ? ' btn-block':'';
				}

				$this->processedData['typolinkButtonClass'] = trim($typolinkButtonClass);
			}

			/**
			 * Textmedia / Textpic / Image
			 */
			if ( $this->processedData['data']['CType'] == 'textmedia'
			 || $this->processedData['data']['CType'] == 'textpic'
			 || $this->processedData['data']['CType'] == 'image' ) {

				 if ($this->processedData['data']['assets'] || $this->processedData['data']['image']) {

					// Gallery row with 25, 50, 75 or 100%
					if ( $this->processedData['data']['tx_t3sbootstrap_inTextImgRowWidth'] == 'auto' ) {
						$this->processedData['rowwidth'] = '';
						$this->processedData['restrowwidth'] = '';
					} else {
						$this->processedData['rowwidth'] = ' '.$this->processedData['data']['tx_t3sbootstrap_inTextImgRowWidth'];

						switch ( $this->processedData['data']['tx_t3sbootstrap_inTextImgRowWidth'] ) {
							 case 'w-25':
							 	$this->processedData['restrowwidth'] = ' w-75';
							break;
							 case 'w-50':
							 	$this->processedData['restrowwidth'] = ' w-50';
							break;
							 case 'w-75':
							 	$this->processedData['restrowwidth'] = ' w-25';
							break;
							 case 'w-100':
							 	$this->processedData['restrowwidth'] = '';
							break;
							default:
								$this->processedData['restrowwidth'] = '';
						}
					}

					$imageorient = $this->processedData['data']['imageorient'];

					$galleryClass = 'gallery text-center';

					// Above or below (0,1,2,8,9)
					if ( $imageorient < 11 ) {
						if ( $imageorient == 0 || $imageorient == 8 ) {
							// center
							$galleryClass .= ' mx-auto '.$this->processedData['rowwidth'];
						}
						if ( $imageorient == 1 || $imageorient == 9 ) {
							// right
							$galleryClass .= ' float-right '.$this->processedData['rowwidth'];
						}
						if ( $imageorient == 2 || $imageorient == 10 ) {
							// left
							$galleryClass .= ' '.$this->processedData['rowwidth'];
						}
					}
					// In Text right or left (17,18)
					if ( $imageorient == 17 || $imageorient == 18 ) {
						$galleryClass .= $imageorient == 17 ? ' float-md-right ml-md-3' : ' float-md-left mr-md-3';
						$galleryClass .= ' '.$this->processedData['rowwidth'];
					}
					// Beside Text right or left (nowrap) (25,26)
					if ( $imageorient == 25 || $imageorient == 26 ) {
						$galleryClass .= ' mx-auto';
					}
					// Beside Text right or left (align-items-center) (66,77)
					if ( $imageorient == 66 || $imageorient == 77 ) {
						$galleryClass .= $imageorient == 17 ? 'float-md-right ml-md-3' : 'float-md-left mr-md-3';
					}

					$this->processedData['galleryClass'] = trim($galleryClass);

					if ( $imageorient > 16 ) {
						$this->processedData['header_beside'] = $this->processedData['data']['tx_t3sbootstrap_header_position'] == 'beside' ? TRUE : FALSE;
					}

					// add media class
					$this->processedData['addmedia']['imgclass'] = 'img-fluid';
					if ( $GLOBALS['_GET']['type'] == '98' ) {
						$this->processedData['addmedia']['imgclass'] .=	'';
					} else {
						$this->processedData['addmedia']['imgclass'] .=	self::getConfigurationValue('lazyLoad') == 1 ? ' lazy' :'';
					}
					$this->processedData['addmedia']['imgclass'] .=	 $this->processedData['data']['imageborder'] ? ' border' :'';
					$this->processedData['addmedia']['imgclass'] .=	$this->processedData['data']['tx_t3sbootstrap_bordercolor']
					 ? ' border-'.$this->processedData['data']['tx_t3sbootstrap_bordercolor'] : '';
					$this->processedData['addmedia']['imagezoom'] =	 $this->processedData['data']['image_zoom'];
					$this->processedData['addmedia']['CType'] =	 $this->processedData['data']['CType'];
				}

			}

			/**
			 * Card
			 */
			if ( $this->processedData['data']['CType'] == 't3sbs_card' ) {
				$this->processedData['addmedia']['imgclass'] = $this->processedData['card']['image']['class'];
				if ( $GLOBALS['_GET']['type'] == '98' ) {
					$this->processedData['addmedia']['imgclass'] .=	'';
				} else {
					$this->processedData['addmedia']['imgclass'] .=	self::getConfigurationValue('lazyLoad') == 1 ? ' lazy' :'';
				}
				$this->processedData['addmedia']['imgclass'] .=	 $this->processedData['data']['imageborder'] ? ' border' :'';
				$this->processedData['addmedia']['figureclass'] =  $this->processedData['data']['image_zoom'] ? ' text-center gallery' : ' text-center';
				$this->processedData['addmedia']['imagezoom'] =	 $this->processedData['data']['image_zoom'];
				$this->processedData['addmedia']['CType'] =	 $this->processedData['data']['CType'];
				$this->processedData['cardSlider'] = $this->parentflexconf['card_wrapper'] == 'slider' ? TRUE : FALSE;
			}

			/**
			 * Media object
			 */
			if ( $this->processedData['data']['CType'] == 't3sbs_mediaobject' ) {

				$this->processedData['mediaobject']['order'] = $this->flexconf['order'] == 'right' ? 'right' : 'left';

				$figureclass = $this->flexconf['order'] == 'right' ? 'd-flex ml-3' : 'd-flex mr-3';

				switch ( $this->processedData['data']['imageorient'] ) {
						 case 91:
						 	$figureclass .= ' align-self-center';
						break;
						 case 92:
						 	$figureclass .= ' align-self-start';
						break;
						 case 93:
						 	$figureclass .= ' align-self-end';
						break;
						 default:
						 	$figureclass .= '';
				}

				$this->processedData['addmedia']['imgclass'] =	$this->processedData['data']['imageborder'] ? 'border' :'';
				if ( $GLOBALS['_GET']['type'] == '98' ) {
					$this->processedData['addmedia']['imgclass'] .=	'';
				} else {
					$this->processedData['addmedia']['imgclass'] .=	self::getConfigurationValue('lazyLoad') == 1 ? ' lazy' :'';
				}
				$this->processedData['addmedia']['figureclass'] =  ' '.trim($figureclass);
				$this->processedData['addmedia']['figureclass'] .=	$this->processedData['data']['image_zoom'] ? ' gallery' : '';
				$this->processedData['addmedia']['imagezoom'] =	 $this->processedData['data']['image_zoom'];
				$this->processedData['addmedia']['CType'] =	 $this->processedData['data']['CType'];
			}

			/**
			 * Carousel
			 */
			if ( $this->processedData['data']['CType'] == 't3sbs_carousel' ) {
				$this->processedData['dimensions']['width'] = $this->parentflexconf['width'] ?: '';
				$this->processedData['dimensions']['height'] = $this->parentflexconf['height'] ?: '';
				$this->processedData['carouselLink'] = $this->parentflexconf['link'];
				$this->processedData['animate'] = $this->parentflexconf['animate'] ? ' caption-animated animated '.$this->parentflexconf['animate'] : '';

				$carouselRatioArr = explode(':', $this->parentflexconf['ratio']);
				if ( !empty($carouselRatioArr[0]) ) {
					$this->processedData['ratio'] = $this->parentflexconf['ratio'];
					if ($this->flexconf['shift']){
						$this->processedData['shift'] = (int)$this->flexconf['shift'] / 100;
					} else {
						$this->processedData['shift'] = '';
					}
				} else {
					$this->processedData['ratio'] = '';
				}
			}

			/**
			 * Table
			 */
			if ( $this->processedData['data']['CType'] == 'table' ) {
				$tableclass = $this->flexconf['tableClass'] ? ' '.$this->flexconf['tableClass']:'';
				$tableclass .= $this->flexconf['tableInverse'] ? ' table-dark' : '';
				$tableclass .= $this->processedData['data']['tx_t3sbootstrap_extra_class'] ? ' '.$this->processedData['data']['tx_t3sbootstrap_extra_class'] : '';
				$this->processedData['tableclass'] = trim($tableclass);
				$this->processedData['tableResponsive'] = $this->flexconf['tableResponsive'] ? TRUE : FALSE;
			}

			$class = self::getClass();

		} // end else CType
		##############################################################################################################################################


		/**
		 * CType: All
		 */
		$allClass = self::getAllClass();

		$celinkClass = '';
		if ($this->processedData['data']['tx_t3sbootstrap_header_celink'] && $this->processedData['data']['header_link']) {
			$this->processedData['celink'] = $this->processedData['data']['header_link'];
			$this->processedData['data']['header_link'] = '';
			$celinkClass = ' ce-link-content';
		}

		$this->processedData['class'] = trim($class.$celinkClass.' '.$allClass);
		$this->processedData['style'] = self::getStyle();
		$this->processedData['anchor'] = FALSE;

		if ( $this->processedData['data']['tx_t3sbootstrap_header_fontawesome'] ) {
			$this->processedData['hFa'] = '<i class="mr-1 '.trim($this->processedData['data']['tx_t3sbootstrap_header_fontawesome']).'"></i> ';
		}

		/**
		 * Header class
		 */
		$hClass = $this->processedData['data']['tx_t3sbootstrap_header_display'] ?: '';

		foreach ( explode( ' ', $this->processedData['data']['tx_t3sbootstrap_header_class']) as $hc ) {

			if ( substr($hc, 0, 5) == 'text-' ) {
				if ( $this->processedData['data']['header_link'] ) {
					$hLinkClass .= ' '.$hc;
				} else {
					$hClass .= ' '.$hc;
				}
			} else {
				$headerClass .= $hc ? ' '.$hc : '';
			}
		}

		$this->processedData['headerClass'] = trim($headerClass);
		$this->processedData['headerClass'] .= $this->processedData['data']['header_position'] ? ' text-'.$this->processedData['data']['header_position'] : '';

		$this->processedData['hClass'] = trim($hClass);
		$this->processedData['hLinkClass'] = trim($hLinkClass);

		/**
		 * Container
		 */
		if ( $this->processorConfiguration['container'] && $this->processedData['data']['tx_t3sbootstrap_container'] ) {

			if ( $this->processedData['be_layout'] === 'OneCol' && $this->processedData['data']['colPos'] === 0 ) {

				$pageContainer = self::getFrontendController()->page['tx_t3sbootstrap_container'] ? TRUE : FALSE;

				if ( $pageContainer === FALSE ) {
					$this->processedData['container'] = $this->processedData['data']['tx_t3sbootstrap_container'];
				} else {
					$this->processedData['container'] = FALSE;
				}

			} else {

				if ( $this->processedData['data']['colPos'] > 2 ) {

					switch ( $this->processedData['data']['colPos'] ) {
						// Jumbotron
						case 3:
							if ( self::getConfigurationValue('jumbotronContainer') ) {
								$this->processedData['container'] = FALSE;
							} else {
								$this->processedData['container'] = $this->processedData['data']['tx_t3sbootstrap_container'];
							}
							break;
						// Footer
						case 4:
							if ( self::getConfigurationValue('footerContainer') ) {
								$this->processedData['container'] = FALSE;
							} else {
								$this->processedData['container'] = $this->processedData['data']['tx_t3sbootstrap_container'];
							}
							break;
						// Expanded Content Top
						case 20:
							if ( self::getConfigurationValue('expandedcontentContainertop') ) {
								$this->processedData['container'] = FALSE;
							} else {
								$this->processedData['container'] = $this->processedData['data']['tx_t3sbootstrap_container'];
							}
							break;
						// Expanded Content Bottom
						case 21:
							if ( self::getConfigurationValue('expandedcontentContainerbottom') ) {
								$this->processedData['container'] = FALSE;
							} else {
								$this->processedData['container'] = $this->processedData['data']['tx_t3sbootstrap_container'];
							}
							break;
						default:
							$this->processedData['container'] = $this->processedData['data']['tx_t3sbootstrap_container'];
					}

				} else {
					$this->processedData['container'] = FALSE;
				}
			}

		} else {
			$this->processedData['container'] = FALSE;
		}

		/**
		 * tx_gridelements_backend_layout == container
		 */
		if ( $this->processedData['data']['tx_gridelements_backend_layout'] == 'container' ) {

			if ( $this->processedData['data']['tx_t3sbootstrap_extra_class'] ) {
#			 	$this->processedData['container'] .= ' '.$this->processedData['data']['tx_t3sbootstrap_extra_class'];
			 	$this->processedData['container'] = trim($this->processedData['container']);
			}
			if ( $this->flexconf['flexContainer'] && $this->flexconf['flexExtraClass']) {
#				$this->processedData['class'] .= ' '.$this->flexconf['flexExtraClass'];
			}
		}

		/**
		 * Child of gridelement (autoLayout_row)
		 */
		if ( $this->processedData['data']['parentgrid_tx_gridelements_backend_layout'] == 'autoLayout_row' ) {
			$this->processedData['newLine'] = $this->flexconf['newLine'] ? TRUE : FALSE;
		}

		// anchor - section menu (one-page-layout)
		if ( ($this->processedData['data']['colPos'] === 0 || $this->processedData['data']['parentgrid_colPos'] === 0)
		 && ($this->processedData['data']['pid'] == self::getFrontendController()->id) ) {
			if ( self::getConfigurationValue('sectionmenu')
			 || (self::getConfigurationValue('sidebarleft') == 'Section')
			 || (self::getConfigurationValue('sidebarright') == 'Section') )
			{
			 $this->processedData['anchor'] = TRUE;
			}
		}

		/**
		 * Menu
		 */
		if ( substr($this->processedData['data']['CType'], 0, 4) == 'menu' ) {
			$pageRepository = GeneralUtility::makeInstance(PageRepository::class);
			$menuPage = $pageRepository->getPage($this->processedData['data']['pid']);
			$this->processedData['menudirection'] = $this->flexconf['menudirection'] ? ' '.$this->flexconf['menudirection'] :'';

		}

		/**
		 * Extend flexforms with custom fields
		 */
		if ( is_array($this->flexconf['ffExtra']) ) {
			$this->processedData['ffExtra'] = $this->flexconf['ffExtra'];
		}

		return $this->processedData;
	}


	/**
	 * Returns the CSS-class
	 *
	 * @return string
	 */
	protected function getClass()
	{

		if ( $this->processorConfiguration['cType'] ) {
			$class = 'fsc-default ce-'. $this->processedData['data']['CType'];
		} else {
			$class = '';
		}

		/**
		 * Button
		 */
		if ( $this->processedData['data']['CType'] == 't3sbs_button' ) {
			// if is no child of gridelement (button_group)
			if ( $this->processedData['data']['parentgrid_tx_gridelements_backend_layout'] != 'button_group' ) {
				$class .= $this->processedData['data']['header_position'] ? ' text-'.$this->processedData['data']['header_position']:'';
			}
		}

		/**
		 * Card
		 */
		if ( $this->processedData['data']['CType'] == 't3sbs_card' ) {
			// Data from cardProzessor
			$class .= $this->processedData['card']['class'] ? ' '.$this->processedData['card']['class']:'';
			#$class .= $this->parentflexconf['card_wrapper'] == 'slider' ? ' h-100' :'';
		}

		/**
		 * All
		 */

		// if is child of gridelement (autoLayout_row)
		if ( $this->processedData['data']['parentgrid_tx_gridelements_backend_layout'] == 'autoLayout_row' ) {
			if ( $this->flexconf['gridSystem'] ) {
				switch ( $this->flexconf['gridSystem'] ) {
					 case 'equal':
						$class .= ' col';
					break;
					 case 'column':
						$class .= $this->flexconf['xsColumns'] ? ' col-'.$this->flexconf['xsColumns'] : '';
					break;
					 case 'variable':

					 if ( $this->flexconf['xsColumns'] == 'equal'
						|| $this->flexconf['smColumns'] == 'equal'
						|| $this->flexconf['mdColumns'] == 'equal'
						|| $this->flexconf['lgColumns'] == 'equal'
						|| $this->flexconf['xlColumns'] == 'equal' ) {

						$class .= $this->flexconf['xsColumns'] ? ' col-xs' : '';
						$class .= $this->flexconf['smColumns'] ? ' col-sm' : '';
						$class .= $this->flexconf['mdColumns'] ? ' col-md' : '';
						$class .= $this->flexconf['lgColumns'] ? ' col-lg' : '';
						$class .= $this->flexconf['xlColumns'] ? ' col-xl': '';

					} else {

						$class .= $this->flexconf['xsColumns'] ? ' col-'.$this->flexconf['xsColumns'] : '';
						$class .= $this->flexconf['smColumns'] ? ' col-sm-'.$this->flexconf['smColumns'] : '';
						$class .= $this->flexconf['mdColumns'] ? ' col-md-'.$this->flexconf['mdColumns'] : '';
						$class .= $this->flexconf['lgColumns'] ? ' col-lg-'.$this->flexconf['lgColumns'] : '';
						$class .= $this->flexconf['xlColumns'] ? ' col-xl-'.$this->flexconf['xlColumns'] : '';
					}
					break;
				}
			}
		}


		// if is child of gridelement (container)
		if ( $this->processedData['data']['parentgrid_tx_gridelements_backend_layout'] == 'container' ) {
			if ( $this->parentflexconf['flexContainer'] ) {
				if ($this->flexconf['responsiveVariations']) {
					$class .= $this->flexconf['alignSelf'] ? ' align-self-'.$this->flexconf['responsiveVariations'].'-'.$this->flexconf['flexContainer'] : '';
				} else {
					$class .= $this->flexconf['alignSelf'] ? ' align-self-'.$this->flexconf['alignSelf'] : '';
				}

				$class .= $this->flexconf['autoMargins'] ? ' '.$this->flexconf['autoMargins'].'-auto' : '';
				$class .= $this->flexconf['order'] ? ' order-'.$this->flexconf['order'] : '';
			}
		}


		return trim($class);
	}


	/**
	 * Returns the CSS-class for gridelements
	 *
	 * @return string
	 */
	protected function getGeClass()
	{
		/**
		 * CType: Gridelements
		 */
		if ( $this->processorConfiguration['cType'] ) {
			$class = 'ge ge_'. $this->processedData['data']['tx_gridelements_backend_layout'];
		} else {
			$class = '';
		}

		/**
		 * Background Wrapper
		 */
		if ( $this->processedData['data']['tx_gridelements_backend_layout'] == 'background_wrapper' && $this->isVideo == FALSE ) {
			$class .= $this->flexconf['bgAttachmentFixed'] ? ' background-fixed' : '';
		}

		/**
		 * Button group
		 */
		if ( $this->processedData['data']['tx_gridelements_backend_layout'] == 'button_group' ) {
			$class .= $this->flexconf['vertical'] ? ' btn-group-vertical' : ' btn-group';
			$class .= $this->flexconf['size'] ? ' '.$this->flexconf['size'] : '';
		}

		/**
		 * Auto-layout row/column
		 */
		if ( $this->processedData['data']['tx_gridelements_backend_layout'] == 'autoLayout_row' ) {
			$class .= $this->flexconf['noGutters'] ? ' no-gutters' : '';
			if ($this->flexconf['responsiveVariations']) {
				$class .= $this->flexconf['justify'] ? ' justify-content-'.$this->flexconf['responsiveVariations'].'-'.$this->flexconf['justify'] : '';
				$class .= $this->flexconf['alignItem'] ? ' align-items-'.$this->flexconf['responsiveVariations'].'-'.$this->flexconf['alignItem'] : '';
			} else {
				$class .= $this->flexconf['alignItem'] ? ' align-items-'.$this->flexconf['alignItem'] : '';
				$class .= $this->flexconf['justify'] ? ' justify-content-'.$this->flexconf['justify'] : '';
			}
		}

		/**
		 * Container
		 */
		if ( $this->processedData['data']['tx_gridelements_backend_layout'] == 'container' ) {
			if ($this->flexconf['flexContainer']) {
				if ($this->flexconf['responsiveVariations']) {
					$class .= $this->flexconf['flexContainer'] ? ' d-'.$this->flexconf['responsiveVariations'].'-'.$this->flexconf['flexContainer'] : '';
					$class .= $this->flexconf['direction'] ? ' flex-'.$this->flexconf['responsiveVariations'].'-'.$this->flexconf['direction'] : '';
					$class .= $this->flexconf['justify'] ? ' justify-content-'.$this->flexconf['responsiveVariations'].'-'.$this->flexconf['justify'] : '';
					$class .= $this->flexconf['alignItem'] ? ' align-items-'.$this->flexconf['responsiveVariations'].'-'.$this->flexconf['alignItem'] : '';
					$class .= $this->flexconf['wrap'] ? ' flex-'.$this->flexconf['responsiveVariations'].'-'.$this->flexconf['wrap'] : '';
					$class .= $this->flexconf['alignContent'] ? ' align-content-'.$this->flexconf['responsiveVariations'].'-'.$this->flexconf['alignContent'] : '';
				} else {
					$class .= $this->flexconf['flexContainer'] ? ' d-'.$this->flexconf['flexContainer'] : '';
					$class .= $this->flexconf['direction'] ? ' flex-'.$this->flexconf['direction'] : '';
					$class .= $this->flexconf['justify'] ? ' justify-content-'.$this->flexconf['justify'] : '';
					$class .= $this->flexconf['alignItem'] ? ' align-items-'.$this->flexconf['alignItem'] : '';
					$class .= $this->flexconf['wrap'] ? ' flex-'.$this->flexconf['wrap'] : '';
					$class .= $this->flexconf['alignContent'] ? ' align-content-'.$this->flexconf['alignContent'] : '';
				}
			}
		}

		/**
		 * Grid
		 */
		if ( $this->processedData['data']['tx_gridelements_backend_layout'] == 'two_columns'
		 || $this->processedData['data']['tx_gridelements_backend_layout'] == 'three_columns'
		 || $this->processedData['data']['tx_gridelements_backend_layout'] == 'four_columns'
		 || $this->processedData['data']['tx_gridelements_backend_layout'] == 'six_columns' ) {
			$class .= $this->flexconf['noGutters'] ? ' no-gutters' : '';
			$class .= $this->flexconf['equalHeight'] ? ' row-eq-height' : '';
		}

		return trim($class);
	}


	/**
	 * Returns the CSS-class for all elements
	 *
	 * @return string
	 */
	protected function getAllClass()
	{
		// Spacing: padding
		if ( $this->processedData['data']['tx_t3sbootstrap_padding_sides'] ) {
			// on all 4 sides of the element
			if ( $this->processedData['data']['tx_t3sbootstrap_padding_sides'] == 'blank' ) {
				$class .= ' p-'.$this->processedData['data']['tx_t3sbootstrap_padding_size'];
			} else {
				$class .= ' p'.$this->processedData['data']['tx_t3sbootstrap_padding_sides'].'-'.$this->processedData['data']['tx_t3sbootstrap_padding_size'];
			}
		}
		// Spacing: margin
		if ( $this->processedData['data']['tx_t3sbootstrap_margin_sides'] ) {
			// on all 4 sides of the element
			if ( $this->processedData['data']['tx_t3sbootstrap_margin_sides'] == 'blank' ) {
				$class .= ' m-'.$this->processedData['data']['tx_t3sbootstrap_margin_size'];
			} else {
				$class .= ' m'.$this->processedData['data']['tx_t3sbootstrap_margin_sides'].'-'.$this->processedData['data']['tx_t3sbootstrap_margin_size'];
			}
		}
		// Layout
		if ($this->processedData['data']['layout']) {

			$frontendController = self::getFrontendController();

			$pagesTSconfig = $frontendController->getPagesTSconfig();
			$layout = $this->processedData['data']['layout'];
			$layoutAddItems = $pagesTSconfig['TCEFORM.']['tt_content.']['layout.']['addItems.'];
			$layoutClasses = $pagesTSconfig['TCEFORM.']['tt_content.']['layout.']['classes.'];
			$layoutAltLabels = $pagesTSconfig['TCEFORM.']['tt_content.']['layout.']['altLabels.'];

			if (isset($layoutAddItems) && key($layoutAddItems) === $layout) {
				$class .= ' layout-'.$layout;
			} elseif (isset($layoutAltLabels) && $layoutAltLabels[$layout]) {
				if (isset($layoutClasses) && $layoutClasses[$layout]) {
					$class .= strtolower($layoutClasses[$layout]);
				} else {
					$class .= ' layout-'.str_replace(' ', '-', strtolower($layoutAltLabels[$layout]));
				}
			} else {
				$class .= ' layout-'.$layout;
			}
		}
		// Frame class
		if ($this->processedData['data']['frame_class'] != 'default') {
			$class .= ' frame-'.$this->processedData['data']['frame_class'];
		}
		// Align self
		if ($this->flexconf['responsiveVariations']) {
			$class .= $this->flexconf['alignSelf'] ? ' align-self-'.$this->flexconf['responsiveVariations'].'-'.$this->flexconf['alignSelf'] : '';
		} else {
			$class .= $this->flexconf['alignSelf'] ? ' align-self-'.$this->flexconf['alignSelf'] : '';
		}
		// Text color
		if ( $this->processedData['data']['tx_t3sbootstrap_textcolor'] ) {
			$class .= ' text-'.$this->processedData['data']['tx_t3sbootstrap_textcolor'];
		}
		// Context color
		if ( $this->processedData['data']['tx_t3sbootstrap_contextcolor'] ) {
			$class .= ' bg-'.$this->processedData['data']['tx_t3sbootstrap_contextcolor'];
		}
		// Extra class
		if ( $this->processedData['data']['tx_t3sbootstrap_extra_class'] ) {
			$class .= $this->processedData['data']['tx_t3sbootstrap_extra_class'] ? ' '.$this->processedData['data']['tx_t3sbootstrap_extra_class'] : '';
		}
		// Border
		if ( $this->flexconf['border'] ) {
			if ( $this->flexconf['border'] == 'border' ) {
				$border = 'border';
			} else {
				$border = 'border '.$this->flexconf['border'];
			}
			$class .= ' '.$border.' border-'.$this->flexconf['borderstyle'];
			$class .= $this->flexconf['borderradius'] ? ' '.$this->flexconf['borderradius'] : '';
		}

		// Hiding / Display Elements
		$class .= $this->flexconf['hidden'] ? ' '.$this->flexconf['hidden'] : '';

		// float
		$class .= $this->flexconf['float'] ? ' '.$this->flexconf['float'] : '';

		return trim($class);
	}


	/**
	 * Returns the CSS-style
	 *
	 * @return string
	 */
	protected function getStyle()
	{
		if ( $this->processedData['data']['tx_t3sbootstrap_bgcolor'] && !$this->processedData['data']['tx_t3sbootstrap_contextcolor'] ) {
			$style = ' background-color: '.$this->processedData['data']['tx_t3sbootstrap_bgcolor'].';';
		}
		if ( $this->processedData['card']['style'] && $this->processedData['data']['parentgrid_tx_gridelements_backend_layout'] != 'card_wrapper' ) {
			$style .= ' '.$this->processedData['card']['style'];
		}

		if ( $this->processedData['data']['tx_gridelements_backend_layout'] == 'background_wrapper'
		 && $this->flexconf['contentCenter'] == 0 && $this->isVideo && $this->processedData['data']['tx_gridelements_view_column_0'] ) {
			$style .= $this->flexconf['paddingTop'] ? ' padding-top: '.$this->flexconf['paddingTop'].'rem;' : '';
		} elseif ( $this->processedData['data']['tx_gridelements_backend_layout'] == 'background_wrapper' ) {
			if ( $this->isVideo == FALSE ) {
				$style .= $this->flexconf['paddingTopBottom'] ? ' padding: '.$this->flexconf['paddingTopBottom'].'rem 1rem;' : '';
				$style .= $this->flexconf['imgGrayscale'] ? ' -webkit-filter: grayscale('.$this->flexconf['imgGrayscale'].'%);' : '';
				$style .= $this->flexconf['imgGrayscale'] ? ' filter: grayscale('.$this->flexconf['imgGrayscale'].'%);' : '';
				$style .= $this->flexconf['imgSepia'] ? ' -webkit-filter: sepia('.$this->flexconf['imgSepia'].'%);' : '';
				$style .= $this->flexconf['imgSepia'] ? ' filter: sepia('.$this->flexconf['imgSepia'].'%);' : '';
				$style .= $this->flexconf['imgOpacity'] ? ' -webkit-filter: opacity('.$this->flexconf['imgOpacity'].'%);' : '';
				$style .= $this->flexconf['imgOpacity'] ? ' filter: opacity('.$this->flexconf['imgOpacity'].'%);' : '';
			}
		}

		if ( $this->processedData['data']['tx_gridelements_backend_layout'] == 'parallax_wrapper' ) {
			$style .= $this->flexconf['paddingTopBottom'] ? ' padding: '.$this->flexconf['paddingTopBottom'].'rem 1rem;' : '';
		}

		return trim($style);
	}


	/**
	 * Get configuration value from processorConfiguration
	 * with when $dataArrayKey fallback to value from cObj->data array
	 *
	 * @param string $key
	 * @param string|NULL $dataArrayKey
	 * @return string
	 */
	protected function getConfigurationValue($key, $dataArrayKey = null)
	{
		$defaultValue = '';
		if ($dataArrayKey && isset($this->contentObjectRenderer->data[$dataArrayKey])) {
			$defaultValue = $this->contentObjectRenderer->data[$dataArrayKey];
		}
		return $this->contentObjectRenderer->stdWrapValue(
			$key,
			$this->processorConfiguration,
			$defaultValue
		);
	}


	/**
	 * Returns the frontend controller
	 *
	 * @return TypoScriptFrontendController
	 */
	protected function getFrontendController()
	{
		return $GLOBALS['TSFE'];
	}


}

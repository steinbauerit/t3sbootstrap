<?php
namespace T3SBS\T3sbootstrap\Controller;

/*
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */

use TYPO3\CMS\Core\Page\PageRenderer;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Resource\FileRepository;
use TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface;
use TYPO3\CMS\Frontend\Resource\FilePathSanitizer;

/**
 * ConsentController
 */
class ConsentController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{

	/**
	 * action index
	 *
	 * @return void
	 */
	public function indexAction()
	{

		$cObj = $this->configurationManager->getContentObjectRenderer();

		$currentRecord = $cObj->data['uid'];

		$settings = $this->configurationManager->getConfiguration(
		ConfigurationManagerInterface::CONFIGURATION_TYPE_SETTINGS
		);

		if ( $settings['consent']['cookie'] && isset($_COOKIE['contentconsent_'.$currentRecord]) && $_COOKIE['contentconsent_'.$currentRecord] == 'allow' ) {

			$contentConsent = TRUE;

		} else {

			$contentConsent = FALSE;

			$jsFooterFile = 'EXT:t3sbootstrap/Resources/Public/Scripts/ajax.js';
			$jsFooterFile = GeneralUtility::makeInstance(FilePathSanitizer::class)->sanitize($jsFooterFile);

			$pageRenderer = GeneralUtility::makeInstance(PageRenderer::class);
			$pageRenderer->addInlineSetting('T3SB','lazyLoad', json_encode($settings));
			$pageRenderer->addJsFooterFile($jsFooterFile);

			$fileRepository = GeneralUtility::makeInstance(FileRepository::class);
			$thumbnails = $fileRepository->findByRelation('tt_content', 'consentpreviewimage', $currentRecord);
		}

		$assignedValues = [
			'currentRecord' => $currentRecord,
			'contentConsent' => $contentConsent,
			'thumbnail' => $thumbnails[0],
		];
		$this->view->assignMultiple($assignedValues);
	}


	/**
	 * Displays the selected result with ajax
	 *
	 * @return void
	 */
	public function ajaxAction()
	{
		$post = GeneralUtility::_POST();
		$currentRecord = $post['currentRecord'];

		$settings = $this->configurationManager->getConfiguration(
		ConfigurationManagerInterface::CONFIGURATION_TYPE_SETTINGS
		);

		if ($settings['consent']['cookie']) {
			$cookieExpire = $settings['cookieExpire'] ? (int)$settings['cookieExpire'] : 30;
			setcookie('contentconsent_'.$currentRecord, 'allow', time() + (86400 * $cookieExpire), '/');
		}
	}


}
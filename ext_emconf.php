<?php

/***************************************************************
 * Extension Manager/Repository config file for ext "t3sbootstrap".
 *
 * Auto generated 23-07-2018 17:41
 *
 * Manual updates:
 * Only the data in the array - everything else is removed by next
 * writing. "version" and "dependencies" must not be touched!
 ***************************************************************/

$EM_CONF[$_EXTKEY] = array (
  'title' => 'Bootstrap Components',
  'description' => 'Startup extension to use bootstrap 4 classes, components and more out of the box. Example and info: www.t3sbootstrap.de',
  'category' => 'templates',
  'author' => 'Helmut Hackbarth',
  'author_email' => 'typo3@t3solution.de',
  'state' => 'stable',
  'uploadfolder' => true,
  'createDirs' => '',
  'clearCacheOnLoad' => true,
  'version' => '4.1.2',
  'constraints' =>
  array (
    'depends' =>
    array (
      'typo3' => '9.5.0-9.5.99',
      'gridelements' => '9.0.0-9.99.99',
      'ws_scss' => '1.1.11-1.99.99',
    ),
    'conflicts' =>
    array (
    ),
    'suggests' =>
    array (
    ),
  ),
  'autoload' =>
  array (
    'psr-4' =>
    array (
      'T3SBS\\T3sbootstrap\\' => 'Classes',
    ),
  ),
  'clearcacheonload' => true,
  'author_company' => NULL,
);

